package ru.tsc.denisturovsky.tm.exception.user;

public final class EmailExistsException extends AbstractUserException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
